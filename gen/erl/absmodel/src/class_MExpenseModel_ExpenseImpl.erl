-module(class_MExpenseModel_ExpenseImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Expense">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MExpenseModel_ExpenseImpl,'id'=null,'datestamp'=null,'description'=null,'amount'=null,'programName'=null,'idCoa'=null}).
'init_internal'()->
    #state{}.

 %% model/Expense.abs:20
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/Expense.abs:21
'get_val_internal'(#state{'datestamp'=G},'datestamp')->
    G;
 %% model/Expense.abs:22
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% model/Expense.abs:23
'get_val_internal'(#state{'amount'=G},'amount')->
    G;
 %% model/Expense.abs:24
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
 %% model/Expense.abs:25
'get_val_internal'(#state{'idCoa'=G},'idCoa')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/Expense.abs:20
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/Expense.abs:21
'set_val_internal'(S,'datestamp',V)->
    S#state{'datestamp'=V};
 %% model/Expense.abs:22
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% model/Expense.abs:23
'set_val_internal'(S,'amount',V)->
    S#state{'amount'=V};
 %% model/Expense.abs:24
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V};
 %% model/Expense.abs:25
'set_val_internal'(S,'idCoa',V)->
    S#state{'idCoa'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'datestamp', S#state.'datestamp' }
        , { 'description', S#state.'description' }
        , { 'amount', S#state.'amount' }
        , { 'programName', S#state.'programName' }
        , { 'idCoa', S#state.'idCoa' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/Expense.abs:20--20
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/Expense.abs:21--21
    put(this, C:set_val_internal(get(this),'datestamp',<<""/utf8>>)),
     %% model/Expense.abs:22--22
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% model/Expense.abs:23--23
    put(this, C:set_val_internal(get(this),'amount',0)),
     %% model/Expense.abs:24--24
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
     %% model/Expense.abs:25--25
    put(this, C:set_val_internal(get(this),'idCoa',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/Expense.abs:27
 %% model/Expense.abs:27
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:27--27
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:28
 %% model/Expense.abs:28
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/Expense.abs:28--28
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:29
 %% model/Expense.abs:29
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:29--29
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:30
 %% model/Expense.abs:30
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% model/Expense.abs:30--30
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:31
 %% model/Expense.abs:31
'm_getDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:31--31
        C:get_val_internal(get(this), 'datestamp')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:32
 %% model/Expense.abs:32
'm_setDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_datestamp_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'datestamp' => V_datestamp_0 }),
    try
         %% model/Expense.abs:32--32
        put(this, C:set_val_internal(get(this), 'datestamp',maps:get('datestamp', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:33
 %% model/Expense.abs:33
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:33--33
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:34
 %% model/Expense.abs:34
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% model/Expense.abs:34--34
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:35
 %% model/Expense.abs:35
'm_getAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:35--35
        C:get_val_internal(get(this), 'amount')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:36
 %% model/Expense.abs:36
'm_setAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_amount_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'amount' => V_amount_0 }),
    try
         %% model/Expense.abs:36--36
        put(this, C:set_val_internal(get(this), 'amount',maps:get('amount', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:37
 %% model/Expense.abs:37
'm_getIdCoa'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Expense.abs:37--37
        C:get_val_internal(get(this), 'idCoa')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIdCoa and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Expense.abs:38
 %% model/Expense.abs:38
'm_setIdCoa'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_coa_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'coa' => V_coa_0 }),
    try
         %% model/Expense.abs:38--38
        put(this, C:set_val_internal(get(this), 'idCoa',maps:get('coa', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setIdCoa and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

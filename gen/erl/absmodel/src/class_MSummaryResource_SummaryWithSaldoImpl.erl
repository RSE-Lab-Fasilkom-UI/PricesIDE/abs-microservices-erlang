-module(class_MSummaryResource_SummaryWithSaldoImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"SummaryWithSaldo">>, <<"Summary">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MSummaryResource_SummaryWithSaldoImpl,'id'=null,'datestamp'=null,'description'=null,'income'=null,'expense'=null,'programName'=null,'saldo'=null}).
'init_internal'()->
    #state{}.

 %% resource/SummaryResource.abs:167
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% resource/SummaryResource.abs:168
'get_val_internal'(#state{'datestamp'=G},'datestamp')->
    G;
 %% resource/SummaryResource.abs:169
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% resource/SummaryResource.abs:170
'get_val_internal'(#state{'income'=G},'income')->
    G;
 %% resource/SummaryResource.abs:171
'get_val_internal'(#state{'expense'=G},'expense')->
    G;
 %% resource/SummaryResource.abs:172
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
 %% resource/SummaryResource.abs:173
'get_val_internal'(#state{'saldo'=G},'saldo')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% resource/SummaryResource.abs:167
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% resource/SummaryResource.abs:168
'set_val_internal'(S,'datestamp',V)->
    S#state{'datestamp'=V};
 %% resource/SummaryResource.abs:169
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% resource/SummaryResource.abs:170
'set_val_internal'(S,'income',V)->
    S#state{'income'=V};
 %% resource/SummaryResource.abs:171
'set_val_internal'(S,'expense',V)->
    S#state{'expense'=V};
 %% resource/SummaryResource.abs:172
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V};
 %% resource/SummaryResource.abs:173
'set_val_internal'(S,'saldo',V)->
    S#state{'saldo'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'datestamp', S#state.'datestamp' }
        , { 'description', S#state.'description' }
        , { 'income', S#state.'income' }
        , { 'expense', S#state.'expense' }
        , { 'programName', S#state.'programName' }
        , { 'saldo', S#state.'saldo' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% resource/SummaryResource.abs:167--167
    put(this, C:set_val_internal(get(this),'id',0)),
     %% resource/SummaryResource.abs:168--168
    put(this, C:set_val_internal(get(this),'datestamp',<<""/utf8>>)),
     %% resource/SummaryResource.abs:169--169
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% resource/SummaryResource.abs:170--170
    put(this, C:set_val_internal(get(this),'income',0)),
     %% resource/SummaryResource.abs:171--171
    put(this, C:set_val_internal(get(this),'expense',0)),
     %% resource/SummaryResource.abs:172--172
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
     %% resource/SummaryResource.abs:173--173
    put(this, C:set_val_internal(get(this),'saldo',0)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% resource/SummaryResource.abs:175
 %% resource/SummaryResource.abs:175
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:175--175
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:176
 %% resource/SummaryResource.abs:176
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/SummaryResource.abs:176--176
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:177
 %% resource/SummaryResource.abs:177
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:177--177
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:178
 %% resource/SummaryResource.abs:178
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% resource/SummaryResource.abs:178--178
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:179
 %% resource/SummaryResource.abs:179
'm_getDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:179--179
        C:get_val_internal(get(this), 'datestamp')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:180
 %% resource/SummaryResource.abs:180
'm_setDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_datestamp_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'datestamp' => V_datestamp_0 }),
    try
         %% resource/SummaryResource.abs:180--180
        put(this, C:set_val_internal(get(this), 'datestamp',maps:get('datestamp', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:181
 %% resource/SummaryResource.abs:181
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:181--181
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:182
 %% resource/SummaryResource.abs:182
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% resource/SummaryResource.abs:182--182
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:183
 %% resource/SummaryResource.abs:183
'm_getIncome'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:183--183
        C:get_val_internal(get(this), 'income')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIncome and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:184
 %% resource/SummaryResource.abs:184
'm_setIncome'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_income_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'income' => V_income_0 }),
    try
         %% resource/SummaryResource.abs:184--184
        put(this, C:set_val_internal(get(this), 'income',maps:get('income', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setIncome and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:185
 %% resource/SummaryResource.abs:185
'm_getExpense'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:185--185
        C:get_val_internal(get(this), 'expense')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getExpense and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:186
 %% resource/SummaryResource.abs:186
'm_setExpense'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_expense_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'expense' => V_expense_0 }),
    try
         %% resource/SummaryResource.abs:186--186
        put(this, C:set_val_internal(get(this), 'expense',maps:get('expense', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setExpense and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:187
 %% resource/SummaryResource.abs:187
'm_getSaldo'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:187--187
        C:get_val_internal(get(this), 'saldo')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getSaldo and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:188
 %% resource/SummaryResource.abs:188
'm_setSaldo'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_saldo_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'saldo' => V_saldo_0 }),
    try
         %% resource/SummaryResource.abs:188--188
        put(this, C:set_val_internal(get(this), 'saldo',((maps:get('saldo', get(vars)) + C:get_val_internal(get(this), 'income'))  - C:get_val_internal(get(this), 'expense')) )),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setSaldo and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

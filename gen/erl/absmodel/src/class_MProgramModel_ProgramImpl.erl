-module(class_MProgramModel_ProgramImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Program">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MProgramModel_ProgramImpl,'id'=null,'name'=null,'description'=null,'target'=null,'partner'=null,'logoUrl'=null,'frequency'=null}).
'init_internal'()->
    #state{}.

 %% model/Program.abs:22
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/Program.abs:23
'get_val_internal'(#state{'name'=G},'name')->
    G;
 %% model/Program.abs:24
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% model/Program.abs:25
'get_val_internal'(#state{'target'=G},'target')->
    G;
 %% model/Program.abs:26
'get_val_internal'(#state{'partner'=G},'partner')->
    G;
 %% model/Program.abs:27
'get_val_internal'(#state{'logoUrl'=G},'logoUrl')->
    G;
 %% model/Program.abs:28
'get_val_internal'(#state{'frequency'=G},'frequency')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/Program.abs:22
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/Program.abs:23
'set_val_internal'(S,'name',V)->
    S#state{'name'=V};
 %% model/Program.abs:24
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% model/Program.abs:25
'set_val_internal'(S,'target',V)->
    S#state{'target'=V};
 %% model/Program.abs:26
'set_val_internal'(S,'partner',V)->
    S#state{'partner'=V};
 %% model/Program.abs:27
'set_val_internal'(S,'logoUrl',V)->
    S#state{'logoUrl'=V};
 %% model/Program.abs:28
'set_val_internal'(S,'frequency',V)->
    S#state{'frequency'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'name', S#state.'name' }
        , { 'description', S#state.'description' }
        , { 'target', S#state.'target' }
        , { 'partner', S#state.'partner' }
        , { 'logoUrl', S#state.'logoUrl' }
        , { 'frequency', S#state.'frequency' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/Program.abs:22--22
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/Program.abs:23--23
    put(this, C:set_val_internal(get(this),'name',<<""/utf8>>)),
     %% model/Program.abs:24--24
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% model/Program.abs:25--25
    put(this, C:set_val_internal(get(this),'target',<<""/utf8>>)),
     %% model/Program.abs:26--26
    put(this, C:set_val_internal(get(this),'partner',<<""/utf8>>)),
     %% model/Program.abs:27--27
    put(this, C:set_val_internal(get(this),'logoUrl',<<""/utf8>>)),
     %% model/Program.abs:28--28
    put(this, C:set_val_internal(get(this),'frequency',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/Program.abs:30
 %% model/Program.abs:30
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:30--30
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:31
 %% model/Program.abs:31
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/Program.abs:31--31
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:32
 %% model/Program.abs:32
'm_getName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:32--32
        C:get_val_internal(get(this), 'name')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:33
 %% model/Program.abs:33
'm_setName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0 }),
    try
         %% model/Program.abs:33--33
        put(this, C:set_val_internal(get(this), 'name',maps:get('name', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:34
 %% model/Program.abs:34
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:34--34
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:35
 %% model/Program.abs:35
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% model/Program.abs:35--35
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:36
 %% model/Program.abs:36
'm_getTarget'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:36--36
        C:get_val_internal(get(this), 'target')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getTarget and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:37
 %% model/Program.abs:37
'm_setTarget'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_target_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'target' => V_target_0 }),
    try
         %% model/Program.abs:37--37
        put(this, C:set_val_internal(get(this), 'target',maps:get('target', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setTarget and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:38
 %% model/Program.abs:38
'm_getPartner'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:38--38
        C:get_val_internal(get(this), 'partner')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getPartner and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:39
 %% model/Program.abs:39
'm_setPartner'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_partner_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'partner' => V_partner_0 }),
    try
         %% model/Program.abs:39--39
        put(this, C:set_val_internal(get(this), 'partner',maps:get('partner', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setPartner and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:40
 %% model/Program.abs:40
'm_getLogoUrl'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:40--40
        C:get_val_internal(get(this), 'logoUrl')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getLogoUrl and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:41
 %% model/Program.abs:41
'm_setLogoUrl'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_logoUrl_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'logoUrl' => V_logoUrl_0 }),
    try
         %% model/Program.abs:41--41
        put(this, C:set_val_internal(get(this), 'logoUrl',maps:get('logoUrl', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setLogoUrl and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:42
 %% model/Program.abs:42
'm_getFrequency'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Program.abs:42--42
        C:get_val_internal(get(this), 'frequency')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getFrequency and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Program.abs:43
 %% model/Program.abs:43
'm_setFrequency'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_frequency_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'frequency' => V_frequency_0 }),
    try
         %% model/Program.abs:43--43
        put(this, C:set_val_internal(get(this), 'frequency',maps:get('frequency', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setFrequency and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

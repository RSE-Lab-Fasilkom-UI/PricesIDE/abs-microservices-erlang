-module(class_MAutomaticReportResource_AutomaticReportResourceImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"AutomaticReportResource">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MAutomaticReportResource_AutomaticReportResourceImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% resource/AutomaticReportResource.abs:25
 %% resource/AutomaticReportResource.abs:25
'm_getIncomes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/AutomaticReportResource.abs:26--26
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MIncomeDbImpl_IncomeDbImpl,[[O,DC| Stack]])}),
         %% resource/AutomaticReportResource.abs:27--27
        put(vars, (get(vars))#{'incomes' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MIncomeModel.IncomeImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MIncomeModel.IncomeImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MIncomeModel.IncomeImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/AutomaticReportResource.abs:28--28
        maps:get('incomes', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIncomes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/AutomaticReportResource.abs:31
 %% resource/AutomaticReportResource.abs:31
'm_getExpenses'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/AutomaticReportResource.abs:32--32
        put(vars, (get(vars))#{'expense_orm' => object:new_local(O, Cog,class_MExpenseDbImpl_ExpenseDbImpl,[[O,DC| Stack]])}),
         %% resource/AutomaticReportResource.abs:33--33
        put(vars, (get(vars))#{'expenses' => (fun() -> case maps:get('expense_orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MExpenseModel.ExpenseImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MExpenseModel.ExpenseImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MExpenseModel.ExpenseImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/AutomaticReportResource.abs:34--34
        maps:get('expenses', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getExpenses and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/AutomaticReportResource.abs:37
 %% resource/AutomaticReportResource.abs:37
'm_transFormToChartOfAccount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_incomes_0,V_expenses_0,V_codeLength_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'incomes' => V_incomes_0,
 'expenses' => V_expenses_0,
 'codeLength' => V_codeLength_0 }),
    try
         %% resource/AutomaticReportResource.abs:38--38
        put(vars, (get(vars))#{'entries' => m_ABS_StdLib_funs:f_map(Cog,[],[O,DC| Stack])}),
         %% resource/AutomaticReportResource.abs:40--40
        put(vars, (get(vars))#{'count' => 0}),
         %% resource/AutomaticReportResource.abs:41--41
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),m_ABS_StdLib_funs:f_length(Cog,maps:get('incomes', get(vars)),[O,DC| Stack])) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/AutomaticReportResource.abs:42--42
                put(vars, (get(vars))#{'income' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('incomes', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:43--43
                put(vars, (get(vars))#{'idCoa' => (fun() -> case maps:get('income', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getIdCoa'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getIdCoa'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getIdCoa',[[]],#task_info{method= <<"getIdCoa"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:44--44
                put(vars, (get(vars))#{'code' => builtin:substr(Cog,maps:get('idCoa', get(vars)),0,maps:get('codeLength', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:45--45
                put(vars, (get(vars))#{'maybeEn' => m_ABS_StdLib_funs:f_lookup(Cog,maps:get('entries', get(vars)),maps:get('code', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:47--47
                case m_ABS_StdLib_funs:f_isJust(Cog,maps:get('maybeEn', get(vars)),[O,DC| Stack]) of
                    true ->  %% resource/AutomaticReportResource.abs:48--48
                    put(vars, (get(vars))#{'entry' => m_ABS_StdLib_funs:f_fromJust(Cog,maps:get('maybeEn', get(vars)),[O,DC| Stack])}),
                     %% resource/AutomaticReportResource.abs:50--50
                    put(vars, (get(vars))#{'entryAmount' => (fun() -> case maps:get('entry', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)()}),
                     %% resource/AutomaticReportResource.abs:51--51
                    put(vars, (get(vars))#{'incomeAmount' => (fun() -> case maps:get('income', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)()}),
                     %% resource/AutomaticReportResource.abs:52--52
                    put(vars, (get(vars))#{'total' => (maps:get('entryAmount', get(vars)) + maps:get('incomeAmount', get(vars))) }),
                     %% resource/AutomaticReportResource.abs:54--54
                    T_1 = (fun() -> case maps:get('entry', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_setAmount'(Callee,maps:get('total', get(vars)),[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            V_amount = maps:get('total', get(vars)),
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_setAmount'(Callee, V_amount,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_setAmount',[maps:get('total', get(vars)),[]],#task_info{method= <<"setAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)(),
                    T_1;
                    false ->                  %% resource/AutomaticReportResource.abs:56--56
                put(vars, (get(vars))#{'utility' => object:new_local(O, Cog,class_ABS_Framework_Utility_UtilityImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:57--57
                put(vars, (get(vars))#{'newEntry' => object:new_local(O, Cog,class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:59--59
                put(vars, (get(vars))#{'newAmount' => (fun() -> case maps:get('income', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:60--60
                T_1 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setAmount'(Callee,maps:get('newAmount', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_amount = maps:get('newAmount', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setAmount'(Callee, V_amount,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setAmount',[maps:get('newAmount', get(vars)),[]],#task_info{method= <<"setAmount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1,
                 %% resource/AutomaticReportResource.abs:61--61
                put(vars, (get(vars))#{'codeInt' => (fun() -> case maps:get('utility', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_stringToInteger'(Callee,maps:get('code', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_s = maps:get('code', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_stringToInteger'(Callee, V_s,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_stringToInteger',[maps:get('code', get(vars)),[]],#task_info{method= <<"stringToInteger"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:62--62
                T_2 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setIdChartOfAccount'(Callee,maps:get('codeInt', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_idChartOfAccount = maps:get('codeInt', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setIdChartOfAccount'(Callee, V_idChartOfAccount,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setIdChartOfAccount',[maps:get('codeInt', get(vars)),[]],#task_info{method= <<"setIdChartOfAccount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_2,
                 %% resource/AutomaticReportResource.abs:63--63
                T_3 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setLevel'(Callee,maps:get('codeLength', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_level = maps:get('codeLength', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setLevel'(Callee, V_level,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setLevel',[maps:get('codeLength', get(vars)),[]],#task_info{method= <<"setLevel"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_3,
                 %% resource/AutomaticReportResource.abs:65--65
                put(vars, (get(vars))#{'entries' := m_ABS_StdLib_funs:f_insert(Cog,maps:get('entries', get(vars)),{ dataPair,maps:get('code', get(vars)),maps:get('newEntry', get(vars))},[O,DC| Stack])})
                end,
                 %% resource/AutomaticReportResource.abs:67--67
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/AutomaticReportResource.abs:70--70
        put(vars, (get(vars))#{'count' := 0}),
         %% resource/AutomaticReportResource.abs:71--71
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),m_ABS_StdLib_funs:f_length(Cog,maps:get('expenses', get(vars)),[O,DC| Stack])) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/AutomaticReportResource.abs:72--72
                put(vars, (get(vars))#{'expense' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('expenses', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:73--73
                put(vars, (get(vars))#{'idCoa' => (fun() -> case maps:get('expense', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getIdCoa'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getIdCoa'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getIdCoa',[[]],#task_info{method= <<"getIdCoa"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:74--74
                put(vars, (get(vars))#{'code' => builtin:substr(Cog,maps:get('idCoa', get(vars)),0,maps:get('codeLength', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:75--75
                put(vars, (get(vars))#{'maybeEn' => m_ABS_StdLib_funs:f_lookup(Cog,maps:get('entries', get(vars)),maps:get('code', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:77--77
                case m_ABS_StdLib_funs:f_isJust(Cog,maps:get('maybeEn', get(vars)),[O,DC| Stack]) of
                    true ->  %% resource/AutomaticReportResource.abs:78--78
                    put(vars, (get(vars))#{'entry' => m_ABS_StdLib_funs:f_fromJust(Cog,maps:get('maybeEn', get(vars)),[O,DC| Stack])}),
                     %% resource/AutomaticReportResource.abs:79--79
                    put(vars, (get(vars))#{'entryAmount' => (fun() -> case maps:get('entry', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)()}),
                     %% resource/AutomaticReportResource.abs:80--80
                    put(vars, (get(vars))#{'expenseAmount' => (fun() -> case maps:get('expense', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)()}),
                     %% resource/AutomaticReportResource.abs:81--81
                    put(vars, (get(vars))#{'total' => (maps:get('entryAmount', get(vars)) + maps:get('expenseAmount', get(vars))) }),
                     %% resource/AutomaticReportResource.abs:83--83
                    T_1 = (fun() -> case maps:get('entry', get(vars)) of
                        null -> throw(dataNullPointerException);
                        Callee=#object{oid=Oid,cog=Cog} ->
                            %% self-call
                            Vars=get(vars),
                            Result=C:'m_setAmount'(Callee,maps:get('total', get(vars)),[O,DC,Vars| Stack]),
                            put(vars, Vars),
                            Result;
                        Callee=#object{oid=ObjRef,cog=Cog} ->
                            %% cog-local call
                            V_amount = maps:get('total', get(vars)),
                            State=get(this),
                            Vars=get(vars),
                            cog:object_state_changed(Cog, O, State),
                            put(this,cog:get_object_state(Callee#object.cog, Callee)),
                            put(task_info,(get(task_info))#task_info{this=Callee}),
                            T=object:get_class_from_state(get(this)), % it's the callee state already
                            Result=T:'m_setAmount'(Callee, V_amount,[O,DC,Vars,State| Stack]),
                            cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                            put(task_info,(get(task_info))#task_info{this=O}),
                            put(this, cog:get_object_state(Cog, O)),
                            put(vars, Vars),
                            Result;
                        Callee ->
                            %% remote call
                            TempFuture = future:start(Callee,'m_setAmount',[maps:get('total', get(vars)),[]],#task_info{method= <<"setAmount"/utf8>>},Cog,[O,DC| Stack]),
                            future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                    end end)(),
                    T_1;
                    false ->                  %% resource/AutomaticReportResource.abs:85--85
                put(vars, (get(vars))#{'utility' => object:new_local(O, Cog,class_ABS_Framework_Utility_UtilityImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:86--86
                put(vars, (get(vars))#{'newEntry' => object:new_local(O, Cog,class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:88--88
                put(vars, (get(vars))#{'newAmount' => (fun() -> case maps:get('expense', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:89--89
                T_1 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setAmount'(Callee,maps:get('newAmount', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_amount = maps:get('newAmount', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setAmount'(Callee, V_amount,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setAmount',[maps:get('newAmount', get(vars)),[]],#task_info{method= <<"setAmount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1,
                 %% resource/AutomaticReportResource.abs:90--90
                put(vars, (get(vars))#{'codeInt' => (fun() -> case maps:get('utility', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_stringToInteger'(Callee,maps:get('code', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_s = maps:get('code', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_stringToInteger'(Callee, V_s,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_stringToInteger',[maps:get('code', get(vars)),[]],#task_info{method= <<"stringToInteger"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:91--91
                T_2 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setIdChartOfAccount'(Callee,maps:get('codeInt', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_idChartOfAccount = maps:get('codeInt', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setIdChartOfAccount'(Callee, V_idChartOfAccount,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setIdChartOfAccount',[maps:get('codeInt', get(vars)),[]],#task_info{method= <<"setIdChartOfAccount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_2,
                 %% resource/AutomaticReportResource.abs:92--92
                T_3 = (fun() -> case maps:get('newEntry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setLevel'(Callee,maps:get('codeLength', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_level = maps:get('codeLength', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setLevel'(Callee, V_level,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setLevel',[maps:get('codeLength', get(vars)),[]],#task_info{method= <<"setLevel"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_3,
                 %% resource/AutomaticReportResource.abs:94--94
                put(vars, (get(vars))#{'entries' := m_ABS_StdLib_funs:f_insert(Cog,maps:get('entries', get(vars)),{ dataPair,maps:get('code', get(vars)),maps:get('newEntry', get(vars))},[O,DC| Stack])})
                end,
                 %% resource/AutomaticReportResource.abs:96--96
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/AutomaticReportResource.abs:98--98
        put(vars, (get(vars))#{'entryList' => m_ABS_StdLib_funs:f_values(Cog,maps:get('entries', get(vars)),[O,DC| Stack])}),
         %% resource/AutomaticReportResource.abs:100--100
        maps:get('entryList', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method transFormToChartOfAccount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/AutomaticReportResource.abs:103
 %% resource/AutomaticReportResource.abs:103
'm_addDetail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_coaSheets_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'coaSheets' => V_coaSheets_0 }),
    try
         %% resource/AutomaticReportResource.abs:104--104
        put(vars, (get(vars))#{'count' => 0}),
         %% resource/AutomaticReportResource.abs:105--105
        put(vars, (get(vars))#{'length' => m_ABS_StdLib_funs:f_length(Cog,maps:get('coaSheets', get(vars)),[O,DC| Stack])}),
         %% resource/AutomaticReportResource.abs:106--106
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),maps:get('length', get(vars))) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/AutomaticReportResource.abs:107--107
                put(vars, (get(vars))#{'utility' => object:new_local(O, Cog,class_ABS_Framework_Utility_UtilityImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:108--108
                put(vars, (get(vars))#{'entry' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('coaSheets', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:110--110
                put(vars, (get(vars))#{'idInt' => (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getIdChartOfAccount'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getIdChartOfAccount'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getIdChartOfAccount',[[]],#task_info{method= <<"getIdChartOfAccount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:111--111
                put(vars, (get(vars))#{'id' => builtin:toString(Cog,maps:get('idInt', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:112--112
                put(vars, (get(vars))#{'id' := iolist_to_binary([maps:get('id', get(vars)), <<"0000"/utf8>>])}),
                 %% resource/AutomaticReportResource.abs:113--113
                put(vars, (get(vars))#{'id' := builtin:substr(Cog,maps:get('id', get(vars)),0,5)}),
                 %% resource/AutomaticReportResource.abs:114--114
                put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
                 %% resource/AutomaticReportResource.abs:116--116
                put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MChartOfAccountDbImpl_ChartOfAccountDbImpl,[[O,DC| Stack]])}),
                 %% resource/AutomaticReportResource.abs:117--117
                put(vars, (get(vars))#{'chartOfAccount' => (fun() -> case maps:get('orm', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_findByAttributes'(Callee,<<"MChartOfAccountModel.ChartOfAccountImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_className = <<"MChartOfAccountModel.ChartOfAccountImpl_c"/utf8>>,
                        V_query = maps:get('condition', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_findByAttributes',[<<"MChartOfAccountModel.ChartOfAccountImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:119--119
                put(vars, (get(vars))#{'description' => (fun() -> case maps:get('chartOfAccount', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getDescription'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getDescription'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getDescription',[[]],#task_info{method= <<"getDescription"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:120--120
                T_1 = (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setDescription'(Callee,maps:get('description', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_description = maps:get('description', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setDescription'(Callee, V_description,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setDescription',[maps:get('description', get(vars)),[]],#task_info{method= <<"setDescription"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1,
                 %% resource/AutomaticReportResource.abs:121--121
                put(vars, (get(vars))#{'name' => (fun() -> case maps:get('chartOfAccount', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getName'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getName'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getName',[[]],#task_info{method= <<"getName"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:122--122
                T_2 = (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setName'(Callee,maps:get('name', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_name = maps:get('name', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setName'(Callee, V_name,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setName',[maps:get('name', get(vars)),[]],#task_info{method= <<"setName"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_2,
                 %% resource/AutomaticReportResource.abs:123--123
                put(vars, (get(vars))#{'code' => (fun() -> case maps:get('utility', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_stringToInteger'(Callee,maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_s = maps:get('id', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_stringToInteger'(Callee, V_s,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_stringToInteger',[maps:get('id', get(vars)),[]],#task_info{method= <<"stringToInteger"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:124--124
                T_3 = (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setIdChartOfAccount'(Callee,maps:get('code', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_idChartOfAccount = maps:get('code', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setIdChartOfAccount'(Callee, V_idChartOfAccount,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setIdChartOfAccount',[maps:get('code', get(vars)),[]],#task_info{method= <<"setIdChartOfAccount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_3,
                 %% resource/AutomaticReportResource.abs:126--126
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/AutomaticReportResource.abs:129--129
        maps:get('coaSheets', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method addDetail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/AutomaticReportResource.abs:132
 %% resource/AutomaticReportResource.abs:132
'm_filterByCodeLevelHelper'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_coaList_0,V_maxLevelLength_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'coaList' => V_coaList_0,
 'maxLevelLength' => V_maxLevelLength_0 }),
    try
         %% resource/AutomaticReportResource.abs:133--133
        put(vars, (get(vars))#{'coaSheetsFinal' => m_ABS_StdLib_funs:f_list(Cog,[],[O,DC| Stack])}),
         %% resource/AutomaticReportResource.abs:135--135
        put(vars, (get(vars))#{'count' => 1}),
         %% resource/AutomaticReportResource.abs:136--136
        []=(fun Loop ([])->
            case cmp:le(maps:get('count', get(vars)),6) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/AutomaticReportResource.abs:137--137
                put(vars, (get(vars))#{'countStr' => builtin:toString(Cog,maps:get('count', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:138--138
                put(vars, (get(vars))#{'coaSheetsTmp' => (fun() -> case O of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_filterByCodeLevel'(Callee,maps:get('coaList', get(vars)),maps:get('countStr', get(vars)),maps:get('maxLevelLength', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_coaList = maps:get('coaList', get(vars)),
                        V_prefixId = maps:get('countStr', get(vars)),
                        V_maxLevelLength = maps:get('maxLevelLength', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_filterByCodeLevel'(Callee, V_coaList, V_prefixId, V_maxLevelLength,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_filterByCodeLevel',[maps:get('coaList', get(vars)),maps:get('countStr', get(vars)),maps:get('maxLevelLength', get(vars)),[]],#task_info{method= <<"filterByCodeLevel"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:139--139
                put(vars, (get(vars))#{'coaSheetsFinal' := m_ABS_StdLib_funs:f_concatenate(Cog,maps:get('coaSheetsFinal', get(vars)),maps:get('coaSheetsTmp', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:140--140
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/AutomaticReportResource.abs:143--143
        maps:get('coaSheetsFinal', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method filterByCodeLevelHelper and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/AutomaticReportResource.abs:146
 %% resource/AutomaticReportResource.abs:146
'm_filterByCodeLevel'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_coaList_0,V_prefixId_0,V_maxLevelLength_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'coaList' => V_coaList_0,
 'prefixId' => V_prefixId_0,
 'maxLevelLength' => V_maxLevelLength_0 }),
    try
         %% resource/AutomaticReportResource.abs:147--147
        put(vars, (get(vars))#{'entries' => m_ABS_StdLib_funs:f_list(Cog,[],[O,DC| Stack])}),
         %% resource/AutomaticReportResource.abs:148--148
        put(vars, (get(vars))#{'maxDigitIndex' => builtin:strlen(Cog,maps:get('prefixId', get(vars)))}),
         %% resource/AutomaticReportResource.abs:149--149
        put(vars, (get(vars))#{'count' => 0}),
         %% resource/AutomaticReportResource.abs:151--151
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),m_ABS_StdLib_funs:f_length(Cog,maps:get('coaList', get(vars)),[O,DC| Stack])) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/AutomaticReportResource.abs:152--152
                put(vars, (get(vars))#{'entry' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('coaList', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/AutomaticReportResource.abs:154--154
                put(vars, (get(vars))#{'entryLevel' => (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getLevel'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getLevel'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getLevel',[[]],#task_info{method= <<"getLevel"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:156--156
                put(vars, (get(vars))#{'codeInt' => (fun() -> case maps:get('entry', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getIdChartOfAccount'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getIdChartOfAccount'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getIdChartOfAccount',[[]],#task_info{method= <<"getIdChartOfAccount"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% resource/AutomaticReportResource.abs:157--157
                put(vars, (get(vars))#{'codeStr' => builtin:toString(Cog,maps:get('codeInt', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:158--158
                put(vars, (get(vars))#{'codeSubstr' => builtin:substr(Cog,maps:get('codeStr', get(vars)),0,maps:get('maxDigitIndex', get(vars)))}),
                 %% resource/AutomaticReportResource.abs:159--159
                put(vars, (get(vars))#{'childDigitLength' => (maps:get('maxDigitIndex', get(vars)) + 2) }),
                 %% resource/AutomaticReportResource.abs:161--161
                case cmp:eq(maps:get('codeSubstr', get(vars)),maps:get('prefixId', get(vars))) of
                    true ->  %% resource/AutomaticReportResource.abs:162--162
                    case cmp:eq(maps:get('entryLevel', get(vars)),maps:get('maxDigitIndex', get(vars))) of
                        true ->  %% resource/AutomaticReportResource.abs:163--163
                        put(vars, (get(vars))#{'entries' := m_ABS_StdLib_funs:f_appendright(Cog,maps:get('entries', get(vars)),maps:get('entry', get(vars)),[O,DC| Stack])});
                        false ->                      %% resource/AutomaticReportResource.abs:164--164
                    case (cmp:le(maps:get('entryLevel', get(vars)),maps:get('childDigitLength', get(vars)))) and (cmp:le(maps:get('childDigitLength', get(vars)),maps:get('maxLevelLength', get(vars)))) of
                        true ->  %% resource/AutomaticReportResource.abs:165--165
                        put(vars, (get(vars))#{'childLevelCode' => builtin:substr(Cog,maps:get('codeStr', get(vars)),0,maps:get('childDigitLength', get(vars)))}),
                         %% resource/AutomaticReportResource.abs:166--166
                        put(vars, (get(vars))#{'childEntries' => (fun() -> case O of
                            null -> throw(dataNullPointerException);
                            Callee=#object{oid=Oid,cog=Cog} ->
                                %% self-call
                                Vars=get(vars),
                                Result=C:'m_filterByCodeLevel'(Callee,maps:get('coaList', get(vars)),maps:get('childLevelCode', get(vars)),maps:get('maxLevelLength', get(vars)),[O,DC,Vars| Stack]),
                                put(vars, Vars),
                                Result;
                            Callee=#object{oid=ObjRef,cog=Cog} ->
                                %% cog-local call
                                V_coaList = maps:get('coaList', get(vars)),
                                V_prefixId = maps:get('childLevelCode', get(vars)),
                                V_maxLevelLength = maps:get('maxLevelLength', get(vars)),
                                State=get(this),
                                Vars=get(vars),
                                cog:object_state_changed(Cog, O, State),
                                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                                put(task_info,(get(task_info))#task_info{this=Callee}),
                                T=object:get_class_from_state(get(this)), % it's the callee state already
                                Result=T:'m_filterByCodeLevel'(Callee, V_coaList, V_prefixId, V_maxLevelLength,[O,DC,Vars,State| Stack]),
                                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                                put(task_info,(get(task_info))#task_info{this=O}),
                                put(this, cog:get_object_state(Cog, O)),
                                put(vars, Vars),
                                Result;
                            Callee ->
                                %% remote call
                                TempFuture = future:start(Callee,'m_filterByCodeLevel',[maps:get('coaList', get(vars)),maps:get('childLevelCode', get(vars)),maps:get('maxLevelLength', get(vars)),[]],#task_info{method= <<"filterByCodeLevel"/utf8>>},Cog,[O,DC| Stack]),
                                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                        end end)()}),
                         %% resource/AutomaticReportResource.abs:167--167
                        put(vars, (get(vars))#{'entries' := m_ABS_StdLib_funs:f_concatenate(Cog,maps:get('entries', get(vars)),maps:get('childEntries', get(vars)),[O,DC| Stack])});
                        false ->                     ok
                    end
                    end;
                    false ->                 ok
                end,
                 %% resource/AutomaticReportResource.abs:171--171
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/AutomaticReportResource.abs:174--174
        maps:get('entries', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method filterByCodeLevel and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

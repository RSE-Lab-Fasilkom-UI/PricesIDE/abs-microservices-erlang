-module(class_MIndividualBeneficiaryModel_IndividualBeneficiaryImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"IndividualBeneficiary">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MIndividualBeneficiaryModel_IndividualBeneficiaryImpl,'id'=null,'name'=null,'address'=null}).
'init_internal'()->
    #state{}.

 %% model/IndividualBeneficiary.abs:14
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/IndividualBeneficiary.abs:15
'get_val_internal'(#state{'name'=G},'name')->
    G;
 %% model/IndividualBeneficiary.abs:16
'get_val_internal'(#state{'address'=G},'address')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/IndividualBeneficiary.abs:14
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/IndividualBeneficiary.abs:15
'set_val_internal'(S,'name',V)->
    S#state{'name'=V};
 %% model/IndividualBeneficiary.abs:16
'set_val_internal'(S,'address',V)->
    S#state{'address'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'name', S#state.'name' }
        , { 'address', S#state.'address' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/IndividualBeneficiary.abs:14--14
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/IndividualBeneficiary.abs:15--15
    put(this, C:set_val_internal(get(this),'name',<<""/utf8>>)),
     %% model/IndividualBeneficiary.abs:16--16
    put(this, C:set_val_internal(get(this),'address',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/IndividualBeneficiary.abs:18
 %% model/IndividualBeneficiary.abs:18
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/IndividualBeneficiary.abs:18--18
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/IndividualBeneficiary.abs:19
 %% model/IndividualBeneficiary.abs:19
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/IndividualBeneficiary.abs:19--19
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/IndividualBeneficiary.abs:20
 %% model/IndividualBeneficiary.abs:20
'm_getName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/IndividualBeneficiary.abs:20--20
        C:get_val_internal(get(this), 'name')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/IndividualBeneficiary.abs:21
 %% model/IndividualBeneficiary.abs:21
'm_setName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0 }),
    try
         %% model/IndividualBeneficiary.abs:21--21
        put(this, C:set_val_internal(get(this), 'name',maps:get('name', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/IndividualBeneficiary.abs:22
 %% model/IndividualBeneficiary.abs:22
'm_getAddress'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/IndividualBeneficiary.abs:22--22
        C:get_val_internal(get(this), 'address')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAddress and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/IndividualBeneficiary.abs:23
 %% model/IndividualBeneficiary.abs:23
'm_setAddress'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_address_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'address' => V_address_0 }),
    try
         %% model/IndividualBeneficiary.abs:23--23
        put(this, C:set_val_internal(get(this), 'address',maps:get('address', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAddress and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

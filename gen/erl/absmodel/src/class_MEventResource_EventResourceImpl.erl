-module(class_MEventResource_EventResourceImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"EventResource">>, <<"Object">> ].

exported() -> #{ <<"delete">> => { 'm_delete', <<"ABS.StdLib.List<Event>">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"detail">> => { 'm_detail', <<"Event">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"list">> => { 'm_list', <<"ABS.StdLib.List<Event>">>, [ ] }, <<"update">> => { 'm_update', <<"Event">>, [ { <<"name">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"description">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"eventDate">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"partner">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"target">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"logoUrl">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"save">> => { 'm_save', <<"ABS.StdLib.List<Event>">>, [ { <<"name">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"description">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"eventDate">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"partner">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"target">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"logoUrl">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] } }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MEventResource_EventResourceImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% resource/EventResource.abs:20
 %% resource/EventResource.abs:20
'm_list'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/EventResource.abs:21--21
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:22--22
        (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MEventModel.EventImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MEventModel.EventImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MEventModel.EventImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method list and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:25
 %% resource/EventResource.abs:25
'm_detail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/EventResource.abs:27--27
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/EventResource.abs:28--28
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:29--29
        (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MEventModel.EventImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method detail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:32
 %% resource/EventResource.abs:32
'm_create'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/EventResource.abs:33--33
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method create and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:36
 %% resource/EventResource.abs:36
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,V_description_0,V_eventDate_0,V_partner_0,V_target_0,V_logoUrl_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0,
 'description' => V_description_0,
 'eventDate' => V_eventDate_0,
 'partner' => V_partner_0,
 'target' => V_target_0,
 'logoUrl' => V_logoUrl_0 }),
    try
         %% resource/EventResource.abs:37--37
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:38--38
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_save'(Callee,maps:get('name', get(vars)),maps:get('description', get(vars)),maps:get('eventDate', get(vars)),maps:get('partner', get(vars)),maps:get('target', get(vars)),maps:get('logoUrl', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_name = maps:get('name', get(vars)),
                V_description = maps:get('description', get(vars)),
                V_eventDate = maps:get('eventDate', get(vars)),
                V_partner = maps:get('partner', get(vars)),
                V_target = maps:get('target', get(vars)),
                V_logoUrl = maps:get('logoUrl', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_save'(Callee, V_name, V_description, V_eventDate, V_partner, V_target, V_logoUrl,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_save',[maps:get('name', get(vars)),maps:get('description', get(vars)),maps:get('eventDate', get(vars)),maps:get('partner', get(vars)),maps:get('target', get(vars)),maps:get('logoUrl', get(vars)),[]],#task_info{method= <<"save"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/EventResource.abs:39--39
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:42
 %% resource/EventResource.abs:42
'm_edit'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/EventResource.abs:43--43
        put(vars, (get(vars))#{'id' => (fun() -> case maps:get('request', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getInput'(Callee,<<"id"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_key = <<"id"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getInput'(Callee, V_key,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getInput',[<<"id"/utf8>>,[]],#task_info{method= <<"getInput"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/EventResource.abs:44--44
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/EventResource.abs:45--45
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:46--46
        put(vars, (get(vars))#{'event' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MEventModel.EventImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/EventResource.abs:48--48
        put(vars, (get(vars))#{'dataModel' => []}),
         %% resource/EventResource.abs:49--49
        m_ABS_StdLib_funs:f_appendright(Cog,maps:get('dataModel', get(vars)),maps:get('event', get(vars)),[O,DC| Stack])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method edit and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:52
 %% resource/EventResource.abs:52
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,V_description_0,V_eventDate_0,V_partner_0,V_target_0,V_logoUrl_0,V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0,
 'description' => V_description_0,
 'eventDate' => V_eventDate_0,
 'partner' => V_partner_0,
 'target' => V_target_0,
 'logoUrl' => V_logoUrl_0,
 'id' => V_id_0 }),
    try
         %% resource/EventResource.abs:53--53
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:54--54
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_update'(Callee,maps:get('name', get(vars)),maps:get('description', get(vars)),maps:get('eventDate', get(vars)),maps:get('partner', get(vars)),maps:get('target', get(vars)),maps:get('logoUrl', get(vars)),maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_name = maps:get('name', get(vars)),
                V_description = maps:get('description', get(vars)),
                V_eventDate = maps:get('eventDate', get(vars)),
                V_partner = maps:get('partner', get(vars)),
                V_target = maps:get('target', get(vars)),
                V_logoUrl = maps:get('logoUrl', get(vars)),
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_update'(Callee, V_name, V_description, V_eventDate, V_partner, V_target, V_logoUrl, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_update',[maps:get('name', get(vars)),maps:get('description', get(vars)),maps:get('eventDate', get(vars)),maps:get('partner', get(vars)),maps:get('target', get(vars)),maps:get('logoUrl', get(vars)),maps:get('id', get(vars)),[]],#task_info{method= <<"update"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/EventResource.abs:55--55
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_detail'(Callee,maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_detail'(Callee, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_detail',[maps:get('id', get(vars)),[]],#task_info{method= <<"detail"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/EventResource.abs:58
 %% resource/EventResource.abs:58
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/EventResource.abs:60--60
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/EventResource.abs:61--61
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MEventDbImpl_EventDbImpl,[[O,DC| Stack]])}),
         %% resource/EventResource.abs:62--62
        put(vars, (get(vars))#{'event' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MEventModel.EventImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MEventModel.EventImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/EventResource.abs:64--64
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_delete'(Callee,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_condition = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_delete'(Callee, V_condition,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_delete',[maps:get('condition', get(vars)),[]],#task_info{method= <<"delete"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/EventResource.abs:65--65
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

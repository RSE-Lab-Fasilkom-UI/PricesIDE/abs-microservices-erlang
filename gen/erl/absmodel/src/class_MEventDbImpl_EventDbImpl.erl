-module(class_MEventDbImpl_EventDbImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"EventDb">>, <<"Object">> ].

exported() -> #{  }.

table() -> "eventimpl".

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MEventDbImpl_EventDbImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% repository/EventDb.abs:22
 %% repository/EventDb.abs:22
'm_findAllByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/EventDb.abs:23--23
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAllByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:25
 %% repository/EventDb.abs:25
'm_findAll'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/EventDb.abs:26--26
        % []
        orm:findAll(table())
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAll and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:28
 %% repository/EventDb.abs:28
'm_findByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/EventDb.abs:29--29
        object:new_local(O, Cog,class_MEventModel_EventImpl,[[O,DC| Stack]]),
        orm:findByAttributes(table(),binary_to_list(maps:get('query', get(vars))))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:31
 %% repository/EventDb.abs:31
'm_find'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/EventDb.abs:32--32
        object:new_local(O, Cog,class_MEventModel_EventImpl,[[O,DC| Stack]])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method find and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:34
 %% repository/EventDb.abs:34
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,V_description_0,V_eventDate_0,V_partner_0,V_target_0,V_logoUrl_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0,
 'description' => V_description_0,
 'eventDate' => V_eventDate_0,
 'partner' => V_partner_0,
 'target' => V_target_0,
 'logoUrl' => V_logoUrl_0 }),
    try
        ok,
        Name = binary_to_list(maps:get('name', get(vars))),
        Description = binary_to_list(maps:get('description', get(vars))),
        EndDate = binary_to_list(maps:get('endDate', get(vars))),
        Partner = binary_to_list(maps:get('partner', get(vars))),
        Target = binary_to_list(maps:get('target', get(vars))),
        LogoUrl = binary_to_list(maps:get('logoUrl', get(vars))),
        orm:saveEvent(table(), Name, Description, EndDate, Partner, Target, LogoUrl),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:36
 %% repository/EventDb.abs:36
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_condition_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'condition' => V_condition_0 }),
    try
        Condition = binary_to_list(maps:get('condition', get(vars))),
        orm:delete(table(), Condition),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:38
 %% repository/EventDb.abs:38
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,V_description_0,V_eventDate_0,V_partner_0,V_target_0,V_logoUrl_0,V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0,
 'description' => V_description_0,
 'eventDate' => V_eventDate_0,
 'partner' => V_partner_0,
 'target' => V_target_0,
 'logoUrl' => V_logoUrl_0,
 'id' => V_id_0 }),
    try
         %% repository/EventDb.abs:39--39
        object:new_local(O, Cog,class_MEventModel_EventImpl,[[O,DC| Stack]]),
        Name = binary_to_list(maps:get('name', get(vars))),
        Description = binary_to_list(maps:get('description', get(vars))),
        EndDate = binary_to_list(maps:get('endDate', get(vars))),
        Partner = binary_to_list(maps:get('partner', get(vars))),
        Target = binary_to_list(maps:get('target', get(vars))),
        LogoUrl = binary_to_list(maps:get('logoUrl', get(vars))),
        Id = binary_to_list(maps:get('id', get(vars))),
        orm:updateEvent(table(), Name, Description, EndDate, Partner, Target, LogoUrl, "Id="+Id)
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/EventDb.abs:41
 %% repository/EventDb.abs:41
'm_log'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_log_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'log' => V_log_0 }),
    try
         %% repository/EventDb.abs:42--42
        <<"return log"/utf8>>
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method log and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

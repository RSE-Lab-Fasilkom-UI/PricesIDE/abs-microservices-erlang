-module(class_MItemDonationModel_ItemDonationImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ItemDonation">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MItemDonationModel_ItemDonationImpl,'id'=null,'programName'=null,'itemName'=null,'quantity'=null}).
'init_internal'()->
    #state{}.

 %% model/ItemDonation.abs:16
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/ItemDonation.abs:17
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
 %% model/ItemDonation.abs:18
'get_val_internal'(#state{'itemName'=G},'itemName')->
    G;
 %% model/ItemDonation.abs:19
'get_val_internal'(#state{'quantity'=G},'quantity')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/ItemDonation.abs:16
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/ItemDonation.abs:17
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V};
 %% model/ItemDonation.abs:18
'set_val_internal'(S,'itemName',V)->
    S#state{'itemName'=V};
 %% model/ItemDonation.abs:19
'set_val_internal'(S,'quantity',V)->
    S#state{'quantity'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'programName', S#state.'programName' }
        , { 'itemName', S#state.'itemName' }
        , { 'quantity', S#state.'quantity' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/ItemDonation.abs:16--16
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/ItemDonation.abs:17--17
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
     %% model/ItemDonation.abs:18--18
    put(this, C:set_val_internal(get(this),'itemName',<<""/utf8>>)),
     %% model/ItemDonation.abs:19--19
    put(this, C:set_val_internal(get(this),'quantity',0)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/ItemDonation.abs:21
 %% model/ItemDonation.abs:21
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ItemDonation.abs:21--21
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:22
 %% model/ItemDonation.abs:22
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/ItemDonation.abs:22--22
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:23
 %% model/ItemDonation.abs:23
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ItemDonation.abs:23--23
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:24
 %% model/ItemDonation.abs:24
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% model/ItemDonation.abs:24--24
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:25
 %% model/ItemDonation.abs:25
'm_getItemName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ItemDonation.abs:25--25
        C:get_val_internal(get(this), 'itemName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getItemName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:26
 %% model/ItemDonation.abs:26
'm_setItemName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_itemName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'itemName' => V_itemName_0 }),
    try
         %% model/ItemDonation.abs:26--26
        put(this, C:set_val_internal(get(this), 'itemName',maps:get('itemName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setItemName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:27
 %% model/ItemDonation.abs:27
'm_getQuantity'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ItemDonation.abs:27--27
        C:get_val_internal(get(this), 'quantity')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getQuantity and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ItemDonation.abs:28
 %% model/ItemDonation.abs:28
'm_setQuantity'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_quantity_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'quantity' => V_quantity_0 }),
    try
         %% model/ItemDonation.abs:28--28
        put(this, C:set_val_internal(get(this), 'quantity',maps:get('quantity', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setQuantity and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

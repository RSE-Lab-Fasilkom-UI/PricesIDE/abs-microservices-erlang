-module(class_MSummaryResource_SummaryResourceImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"SummaryResource">>, <<"Object">> ].

exported() -> #{ <<"list">> => { 'm_list', <<"ABS.StdLib.List<SummaryWithSaldo>">>, [ ] }, <<"update">> => { 'm_update', <<"Summary">>, [ { <<"datestamp">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"description">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"income">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"expense">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"programName">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"save">> => { 'm_save', <<"ABS.StdLib.List<SummaryWithSaldo>">>, [ { <<"datestamp">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"description">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"income">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"expense">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"programName">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"detail">> => { 'm_detail', <<"Summary">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"delete">> => { 'm_delete', <<"ABS.StdLib.List<SummaryWithSaldo>">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] } }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MSummaryResource_SummaryResourceImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% resource/SummaryResource.abs:29
 %% resource/SummaryResource.abs:29
'm_getIncomes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:30--30
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MIncomeDbImpl_IncomeDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:31--31
        put(vars, (get(vars))#{'incomes' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MIncomeModel.IncomeImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MIncomeModel.IncomeImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MIncomeModel.IncomeImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:32--32
        maps:get('incomes', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIncomes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:35
 %% resource/SummaryResource.abs:35
'm_getExpenses'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:36--36
        put(vars, (get(vars))#{'expense_orm' => object:new_local(O, Cog,class_MExpenseDbImpl_ExpenseDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:37--37
        put(vars, (get(vars))#{'expenses' => (fun() -> case maps:get('expense_orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MExpenseModel.ExpenseImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MExpenseModel.ExpenseImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MExpenseModel.ExpenseImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:38--38
        maps:get('expenses', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getExpenses and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:41
 %% resource/SummaryResource.abs:41
'm_list'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/SummaryResource.abs:42--42
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:44--44
        put(vars, (get(vars))#{'summariesWithSaldo' => []}),
         %% resource/SummaryResource.abs:45--45
        put(vars, (get(vars))#{'allIncomes' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getIncomes'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getIncomes'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getIncomes',[[]],#task_info{method= <<"getIncomes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:46--46
        put(vars, (get(vars))#{'allExpenses' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getExpenses'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getExpenses'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getExpenses',[[]],#task_info{method= <<"getExpenses"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:48--48
        put(vars, (get(vars))#{'count' => 0}),
         %% resource/SummaryResource.abs:49--49
        put(vars, (get(vars))#{'saldo' => 0}),
         %% resource/SummaryResource.abs:51--51
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),m_ABS_StdLib_funs:f_length(Cog,maps:get('allIncomes', get(vars)),[O,DC| Stack])) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/SummaryResource.abs:52--52
                put(vars, (get(vars))#{'summaryWithSaldo' => object:new_local(O, Cog,class_MSummaryResource_SummaryWithSaldoImpl,[[O,DC| Stack]])}),
                 %% resource/SummaryResource.abs:53--53
                put(vars, (get(vars))#{'thisLoopIncome' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('allIncomes', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/SummaryResource.abs:55--55
                A = maps:get('thisLoopIncome', get(vars)),
                IncomeId = binary_to_integer(maps:get(<<"id">>, A)),
                put(vars, (get(vars))#{'incomeId' => IncomeId}),
                % put(vars, (get(vars))#{'incomeId' => (fun() -> case maps:get('thisLoopIncome', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getId'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getId'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getId',[[]],#task_info{method= <<"getId"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack]),
                % end end)()}),
                 %% resource/SummaryResource.abs:56--56
                T_1 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setId'(Callee,maps:get('incomeId', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_id = maps:get('incomeId', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setId'(Callee, V_id,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setId',[maps:get('incomeId', get(vars)),[]],#task_info{method= <<"setId"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1,
                 %% resource/SummaryResource.abs:58--58
                ProgramName = maps:get(<<"programname">>, A),
                put(vars, (get(vars))#{'incomeProgramNames' => ProgramName}),
                % put(vars, (get(vars))#{'incomeProgramNames' => (fun() -> case maps:get('thisLoopIncome', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getProgramName'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getProgramName'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getProgramName',[[]],#task_info{method= <<"getProgramName"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:59--59
                T_2 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setProgramName'(Callee,maps:get('incomeProgramNames', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_programName = maps:get('incomeProgramNames', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setProgramName'(Callee, V_programName,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setProgramName',[maps:get('incomeProgramNames', get(vars)),[]],#task_info{method= <<"setProgramName"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_2,
                 %% resource/SummaryResource.abs:61--61
                IncomeDatestamp = maps:get(<<"datestamp">>, A),
                put(vars, (get(vars))#{'incomeDatestamp' => IncomeDatestamp}),
                % put(vars, (get(vars))#{'incomeDatestamp' => (fun() -> case maps:get('thisLoopIncome', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getDatestamp'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getDatestamp'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getDatestamp',[[]],#task_info{method= <<"getDatestamp"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:62--62
                T_3 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setDatestamp'(Callee,maps:get('incomeDatestamp', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_datestamp = maps:get('incomeDatestamp', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setDatestamp'(Callee, V_datestamp,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setDatestamp',[maps:get('incomeDatestamp', get(vars)),[]],#task_info{method= <<"setDatestamp"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_3,
                 %% resource/SummaryResource.abs:64--64
                IncomeDescription = maps:get(<<"description">>, A),
                put(vars, (get(vars))#{'incomeDescription' => IncomeDescription}),
                % put(vars, (get(vars))#{'incomeDescription' => (fun() -> case maps:get('thisLoopIncome', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getDescription'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getDescription'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getDescription',[[]],#task_info{method= <<"getDescription"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:65--65
                T_4 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setDescription'(Callee,maps:get('incomeDescription', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_description = maps:get('incomeDescription', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setDescription'(Callee, V_description,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setDescription',[maps:get('incomeDescription', get(vars)),[]],#task_info{method= <<"setDescription"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_4,
                 %% resource/SummaryResource.abs:67--67
                IncomeIncome = binary_to_integer(maps:get(<<"amount">>, A)),
                put(vars, (get(vars))#{'incomeIncome' => IncomeIncome}),
                % put(vars, (get(vars))#{'incomeIncome' => (fun() -> case maps:get('thisLoopIncome', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:68--68
                T_5 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setIncome'(Callee,maps:get('incomeIncome', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_income = maps:get('incomeIncome', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setIncome'(Callee, V_income,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setIncome',[maps:get('incomeIncome', get(vars)),[]],#task_info{method= <<"setIncome"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_5,
                 %% resource/SummaryResource.abs:70--70
                T_6 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setExpense'(Callee,0,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_expense = 0,
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setExpense'(Callee, V_expense,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setExpense',[0,[]],#task_info{method= <<"setExpense"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_6,
                 %% resource/SummaryResource.abs:72--72
                put(vars, (get(vars))#{'saldo' := (maps:get('saldo', get(vars)) + maps:get('incomeIncome', get(vars))) }),
                IncomeSaldo = maps:get('saldo', get(vars)) + maps:get('incomeIncome', get(vars)),
                 %% resource/SummaryResource.abs:73--73
                T_7 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setSaldo'(Callee,maps:get('saldo', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_saldo = maps:get('saldo', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setSaldo'(Callee, V_saldo,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setSaldo',[maps:get('saldo', get(vars)),[]],#task_info{method= <<"setSaldo"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_7,
                 %% resource/SummaryResource.abs:74--74
                put(vars, (get(vars))#{'newSummaries' => m_ABS_StdLib_funs:f_appendright(Cog,maps:get('summariesWithSaldo', get(vars)),maps:get('summaryWithSaldo', get(vars)),[O,DC| Stack])}),
                 %% resource/SummaryResource.abs:75--75
                % put(vars, (get(vars))#{'summariesWithSaldo' := maps:get('newSummaries', get(vars))}),
                 %% resource/SummaryResource.abs:76--76
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
                NewIncomeMapList = [#{
                    <<"id">>=>IncomeId,
                    <<"datestamp">>=>IncomeDatestamp,
                    <<"description">>=>IncomeDescription,
                    <<"income">>=>IncomeIncome,
                    <<"expense">>=>0,
                    <<"programName">>=>ProgramName,
                    <<"saldo">>=>IncomeSaldo}],
                OldSummaries = maps:get('summariesWithSaldo', get(vars)),
                NewArr = OldSummaries ++ NewIncomeMapList,
                put(vars, (get(vars))#{'summariesWithSaldo' => NewArr}),
            Loop([])  end end)
        ([]),
         %% resource/SummaryResource.abs:79--79
        put(vars, (get(vars))#{'count' := 0}),
         %% resource/SummaryResource.abs:80--80
        []=(fun Loop ([])->
            case cmp:lt(maps:get('count', get(vars)),m_ABS_StdLib_funs:f_length(Cog,maps:get('allExpenses', get(vars)),[O,DC| Stack])) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% resource/SummaryResource.abs:81--81
                put(vars, (get(vars))#{'summaryWithSaldo' => object:new_local(O, Cog,class_MSummaryResource_SummaryWithSaldoImpl,[[O,DC| Stack]])}),
                 %% resource/SummaryResource.abs:82--82
                put(vars, (get(vars))#{'thisLoopExpense' => m_ABS_StdLib_funs:f_nth(Cog,maps:get('allExpenses', get(vars)),maps:get('count', get(vars)),[O,DC| Stack])}),
                 %% resource/SummaryResource.abs:84--84
                ExpenseLoop = maps:get('thisLoopExpense', get(vars)),
                ExpenseId = binary_to_integer(maps:get(<<"id">>, ExpenseLoop)),
                put(vars, (get(vars))#{'expenseId' => ExpenseId}),
                % put(vars, (get(vars))#{'expenseId' => (fun() -> case maps:get('thisLoopExpense', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getId'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getId'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getId',[[]],#task_info{method= <<"getId"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:85--85
                T_1 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setId'(Callee,maps:get('expenseId', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_id = maps:get('expenseId', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setId'(Callee, V_id,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setId',[maps:get('expenseId', get(vars)),[]],#task_info{method= <<"setId"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1,
                 %% resource/SummaryResource.abs:87--87
                ExpenseProgramNames = maps:get(<<"programname">>, ExpenseLoop),
                put(vars, (get(vars))#{'expenseProgramNames' => ExpenseProgramNames}),
                % put(vars, (get(vars))#{'expenseProgramNames' => (fun() -> case maps:get('thisLoopExpense', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getProgramName'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getProgramName'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getProgramName',[[]],#task_info{method= <<"getProgramName"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:88--88
                T_2 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setProgramName'(Callee,maps:get('expenseProgramNames', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_programName = maps:get('expenseProgramNames', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setProgramName'(Callee, V_programName,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setProgramName',[maps:get('expenseProgramNames', get(vars)),[]],#task_info{method= <<"setProgramName"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_2,
                 %% resource/SummaryResource.abs:90--90
                ExpenseDatestamp = maps:get(<<"datestamp">>, ExpenseLoop),
                put(vars, (get(vars))#{'expenseDatestamp' => ExpenseDatestamp}),
                % put(vars, (get(vars))#{'expenseDatestamp' => (fun() -> case maps:get('thisLoopExpense', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getDatestamp'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getDatestamp'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getDatestamp',[[]],#task_info{method= <<"getDatestamp"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:91--91
                T_3 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setDatestamp'(Callee,maps:get('expenseDatestamp', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_datestamp = maps:get('expenseDatestamp', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setDatestamp'(Callee, V_datestamp,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setDatestamp',[maps:get('expenseDatestamp', get(vars)),[]],#task_info{method= <<"setDatestamp"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_3,
                 %% resource/SummaryResource.abs:93--93
                ExpenseDescription = maps:get(<<"description">>, ExpenseLoop),
                put(vars, (get(vars))#{'expenseDescription' => ExpenseDescription}),
                % put(vars, (get(vars))#{'expenseDescription' => (fun() -> case maps:get('thisLoopExpense', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getDescription'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getDescription'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getDescription',[[]],#task_info{method= <<"getDescription"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:94--94
                T_4 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setDescription'(Callee,maps:get('expenseDescription', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_description = maps:get('expenseDescription', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setDescription'(Callee, V_description,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setDescription',[maps:get('expenseDescription', get(vars)),[]],#task_info{method= <<"setDescription"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_4,
                 %% resource/SummaryResource.abs:96--96
                T_5 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setIncome'(Callee,0,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_income = 0,
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setIncome'(Callee, V_income,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setIncome',[0,[]],#task_info{method= <<"setIncome"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_5,
                 %% resource/SummaryResource.abs:98--98
                ExpenseAmount = binary_to_integer(maps:get(<<"amount">>, ExpenseLoop)),
                put(vars, (get(vars))#{'expenseValue' => ExpenseAmount}),
                % put(vars, (get(vars))#{'expenseValue' => (fun() -> case maps:get('thisLoopExpense', get(vars)) of
                %     null -> throw(dataNullPointerException);
                %     Callee=#object{oid=Oid,cog=Cog} ->
                %         %% self-call
                %         Vars=get(vars),
                %         Result=C:'m_getAmount'(Callee,[O,DC,Vars| Stack]),
                %         put(vars, Vars),
                %         Result;
                %     Callee=#object{oid=ObjRef,cog=Cog} ->
                %         %% cog-local call
                %         State=get(this),
                %         Vars=get(vars),
                %         cog:object_state_changed(Cog, O, State),
                %         put(this,cog:get_object_state(Callee#object.cog, Callee)),
                %         put(task_info,(get(task_info))#task_info{this=Callee}),
                %         T=object:get_class_from_state(get(this)), % it's the callee state already
                %         Result=T:'m_getAmount'(Callee,[O,DC,Vars,State| Stack]),
                %         cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                %         put(task_info,(get(task_info))#task_info{this=O}),
                %         put(this, cog:get_object_state(Cog, O)),
                %         put(vars, Vars),
                %         Result;
                %     Callee ->
                %         %% remote call
                %         TempFuture = future:start(Callee,'m_getAmount',[[]],#task_info{method= <<"getAmount"/utf8>>},Cog,[O,DC| Stack]),
                %         future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                % end end)()}),
                 %% resource/SummaryResource.abs:99--99
                T_6 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setExpense'(Callee,maps:get('expenseValue', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_expense = maps:get('expenseValue', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setExpense'(Callee, V_expense,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setExpense',[maps:get('expenseValue', get(vars)),[]],#task_info{method= <<"setExpense"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_6,
                ExpenseSaldo = maps:get('saldo', get(vars)) - maps:get('expenseValue', get(vars)),
                 %% resource/SummaryResource.abs:101--101
                put(vars, (get(vars))#{'saldo' := (maps:get('saldo', get(vars)) - maps:get('expenseValue', get(vars))) }),
                 %% resource/SummaryResource.abs:102--102
                T_7 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setSaldo'(Callee,maps:get('saldo', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_saldo = maps:get('saldo', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_setSaldo'(Callee, V_saldo,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setSaldo',[maps:get('saldo', get(vars)),[]],#task_info{method= <<"setSaldo"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_7,

                T_100 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_setSaldo'(Callee,maps:get('saldo', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_saldo = maps:get('saldo', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getSaldo'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_setSaldo',[maps:get('saldo', get(vars)),[]],#task_info{method= <<"setSaldo"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                T_1001 = (fun() -> case maps:get('summaryWithSaldo', get(vars)) of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_getDatestamp'(Callee,[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_getId'(Callee,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_getDatestamp',[[]],#task_info{method= <<"getDatestamp"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)(),
                NewExpenseMapList = [#{
                    <<"id">>=>ExpenseId,
                    <<"datestamp">>=>ExpenseDatestamp,
                    <<"description">>=>ExpenseDescription,
                    <<"income">>=>0,
                    <<"expense">>=>ExpenseAmount,
                    <<"programName">>=>ExpenseProgramNames,
                    <<"saldo">>=>ExpenseSaldo}],
                OldSummaries = maps:get('summariesWithSaldo', get(vars)),
                NewArr = OldSummaries ++ NewExpenseMapList,
                put(vars, (get(vars))#{'summariesWithSaldo' => NewArr}),
                 %% resource/SummaryResource.abs:103--103
                % put(vars, (get(vars))#{'summariesWithSaldo' := m_ABS_StdLib_funs:f_appendright(Cog,maps:get('summariesWithSaldo', get(vars)),maps:get('summaryWithSaldo', get(vars)),[O,DC| Stack])}),
                 %% resource/SummaryResource.abs:104--104
                put(vars, (get(vars))#{'count' := (maps:get('count', get(vars)) + 1) }),
            Loop([])  end end)
        ([]),
         %% resource/SummaryResource.abs:108--108
        put(vars, (get(vars))#{'foreignUtility' => object:new_local(O, Cog,class_ABS_Framework_ForeignUtility_ForeignUtilityImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:109--109
        put(vars, (get(vars))#{'sortedSummaries' => (fun() -> case maps:get('foreignUtility', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_sort'(Callee,maps:get('summariesWithSaldo', get(vars)),<<"saldo"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_items = maps:get('summariesWithSaldo', get(vars)),
                V_key = <<"saldo"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_sort'(Callee, V_items, V_key,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_sort',[maps:get('summariesWithSaldo', get(vars)),<<"saldo"/utf8>>,[]],#task_info{method= <<"sort"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:110--110
        % maps:get('sortedSummaries', get(vars))
        maps:get('summariesWithSaldo', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method list and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:113
 %% resource/SummaryResource.abs:113
'm_detail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/SummaryResource.abs:115--115
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/SummaryResource.abs:116--116
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:117--117
        (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MSummaryModel.SummaryImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method detail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:120
 %% resource/SummaryResource.abs:120
'm_create'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/SummaryResource.abs:121--121
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method create and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:124
 %% resource/SummaryResource.abs:124
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_datestamp_0,V_description_0,V_income_0,V_expense_0,V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'datestamp' => V_datestamp_0,
 'description' => V_description_0,
 'income' => V_income_0,
 'expense' => V_expense_0,
 'programName' => V_programName_0 }),
    try
         %% resource/SummaryResource.abs:126--126
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:127--127
        put(vars, (get(vars))#{'summary' => object:new_local(O, Cog,class_MSummaryModel_SummaryImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:129--129
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_save'(Callee,maps:get('datestamp', get(vars)),maps:get('description', get(vars)),maps:get('income', get(vars)),maps:get('expense', get(vars)),maps:get('programName', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_datestamp = maps:get('datestamp', get(vars)),
                V_description = maps:get('description', get(vars)),
                V_income = maps:get('income', get(vars)),
                V_expense = maps:get('expense', get(vars)),
                V_programName = maps:get('programName', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_save'(Callee, V_datestamp, V_description, V_income, V_expense, V_programName,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_save',[maps:get('datestamp', get(vars)),maps:get('description', get(vars)),maps:get('income', get(vars)),maps:get('expense', get(vars)),maps:get('programName', get(vars)),[]],#task_info{method= <<"save"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/SummaryResource.abs:130--130
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:133
 %% resource/SummaryResource.abs:133
'm_edit'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/SummaryResource.abs:134--134
        put(vars, (get(vars))#{'id' => (fun() -> case maps:get('request', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getInput'(Callee,<<"id"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_key = <<"id"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getInput'(Callee, V_key,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getInput',[<<"id"/utf8>>,[]],#task_info{method= <<"getInput"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:135--135
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/SummaryResource.abs:136--136
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:137--137
        put(vars, (get(vars))#{'summary' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MSummaryModel.SummaryImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:139--139
        put(vars, (get(vars))#{'dataModel' => []}),
         %% resource/SummaryResource.abs:140--140
        m_ABS_StdLib_funs:f_appendright(Cog,maps:get('dataModel', get(vars)),maps:get('summary', get(vars)),[O,DC| Stack])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method edit and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:143
 %% resource/SummaryResource.abs:143
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_datestamp_0,V_description_0,V_income_0,V_expense_0,V_programName_0,V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'datestamp' => V_datestamp_0,
 'description' => V_description_0,
 'income' => V_income_0,
 'expense' => V_expense_0,
 'programName' => V_programName_0,
 'id' => V_id_0 }),
    try
         %% resource/SummaryResource.abs:144--144
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/SummaryResource.abs:145--145
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:146--146
        put(vars, (get(vars))#{'summary' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MSummaryModel.SummaryImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MSummaryModel.SummaryImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/SummaryResource.abs:148--148
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_update'(Callee,maps:get('datestamp', get(vars)),maps:get('description', get(vars)),maps:get('income', get(vars)),maps:get('expense', get(vars)),maps:get('programName', get(vars)),maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_datestamp = maps:get('datestamp', get(vars)),
                V_description = maps:get('description', get(vars)),
                V_income = maps:get('income', get(vars)),
                V_expense = maps:get('expense', get(vars)),
                V_programName = maps:get('programName', get(vars)),
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_update'(Callee, V_datestamp, V_description, V_income, V_expense, V_programName, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_update',[maps:get('datestamp', get(vars)),maps:get('description', get(vars)),maps:get('income', get(vars)),maps:get('expense', get(vars)),maps:get('programName', get(vars)),maps:get('id', get(vars)),[]],#task_info{method= <<"update"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/SummaryResource.abs:150--150
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_detail'(Callee,maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_detail'(Callee, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_detail',[maps:get('id', get(vars)),[]],#task_info{method= <<"detail"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/SummaryResource.abs:153
 %% resource/SummaryResource.abs:153
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/SummaryResource.abs:154--154
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/SummaryResource.abs:155--155
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MSummaryDbImpl_SummaryDbImpl,[[O,DC| Stack]])}),
         %% resource/SummaryResource.abs:156--156
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_delete'(Callee,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_condition = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_delete'(Callee, V_condition,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_delete',[maps:get('condition', get(vars)),[]],#task_info{method= <<"delete"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/SummaryResource.abs:157--157
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

abs_to_json(true) -> true;
abs_to_json(false) -> false;
abs_to_json(null) -> null;
abs_to_json(Abs) when is_number(Abs) -> Abs;
abs_to_json(Abs) when is_list(Abs) -> lists:map(fun abs_to_json/1, Abs);
abs_to_json({N, D}) when is_integer(N), is_integer(D) -> N / D;
abs_to_json(Abs) when is_binary(Abs) -> Abs;
abs_to_json(dataEmptySet) -> [];
abs_to_json(Abs={dataInsert, _, _}) ->
    lists:map(fun abs_to_json/1, abs_set_to_erl_list(Abs));
abs_to_json(dataEmptyMap) -> #{};
abs_to_json(Abs={dataInsertAssoc, _, _}) ->
    maps:fold(fun (K, V, Result) ->
                      Result#{
                        case is_binary(K) of
                            true -> K;
                            false -> builtin:toString(null, K)
                        end
                        => abs_to_json(V)}
              end,
              #{},
              abs_map_to_erl_map(Abs));
abs_to_json(Abs) when is_tuple(Abs) ->
    abs_constructor_info:to_json(tuple_to_list(Abs));
abs_to_json(Abs) -> builtin:toString(null, Abs).

%% Convert into JSON integers instead of floats: erlang throws badarith,
%% possibly because of underflow, when the rationals get very large (test
%% case:
%% 5472206596936366950716234513847726699787633130083257868108935385073372521628474400544521868704326539544514945848761641723966834493669011242722490852350250920069840584545494657714176547830170076546766985189948190456085993999965841854043348210273114730931817418950948724982907640273166024155584846472815748114062887634396966520123600001491695765504058451726579037573091051607552055423198699302802395956790501740896358894471037106650700904924688637794684243427125018755079147845309097447199680
%% /
%% 124463949580340014510986584728288862741803258927911335916878977240693013539088417076664562979601325740156255021810491719369575684864053619527117112327603062438063916198087526979602339983985468095190606013682227096265800975172556004113539099465524910539717462241015411708644965352863529428218264513501978734125515935814243527381509019662918750484272597804450713973581541009172909728477620791912354661828439825472308754606657629002302337966418841432399509232916424732092270353567444570118827)
%% Besides, we only want the number to display it in a graph, so the fraction
%% doesn't make a difference
abs_to_erl_number({N, 1}) -> N;
abs_to_erl_number({N, D}) -> N div D;
abs_to_erl_number(I) -> I.

abs_set_to_erl_list(dataEmptySet) -> [];
abs_set_to_erl_list({dataInsert, Item, Set}) -> [Item | abs_set_to_erl_list(Set)].

abs_map_to_erl_map(dataEmptyMap) -> #{};
abs_map_to_erl_map({dataInsertAssoc, {dataPair, Key, Value}, Map}) ->
    Restmap = abs_map_to_erl_map(Map),
    Restmap#{Key => Value}.


convert_number_list(List) ->
    lists:map(fun abs_to_erl_number/1, lists:reverse(List)).

create_history_list({dataTime, StartingTime}, History, Totalhistory) ->
    History2 = convert_number_list(History),
    Totalhistory2 = convert_number_list(Totalhistory),
    StartingTime2 = abs_to_erl_number(StartingTime),
    Length = length(History2),
    Indices=lists:seq(StartingTime2, StartingTime2 + Length - 1),
    case Length == length(Totalhistory2) of
        true -> lists:zipwith3(fun (Ind, His, Tot) -> [Ind, His, Tot] end,
                               Indices, History2, Totalhistory2);
        false -> lists:zipwith(fun (Ind, His) -> [Ind, His] end,
                               Indices, History2)
    end.


get_statistics_json() ->
    DCs = cog_monitor:get_dcs(),
    DC_infos=lists:map(fun (X) -> dc:get_resource_history(X, cpu) end, DCs),
    DC_info_json=lists:map(fun([Description, CreationTime, History, Totalhistory]) ->
                                   [{<<"name">>, Description},
                                    {<<"values">>, create_history_list(CreationTime, History, Totalhistory)}]
                           end, DC_infos),
    io_lib:format("Deployment components:~n~w~n",
                  [jsx:encode(DC_info_json)]),
    jsx:encode(DC_info_json).


atomize(L) when is_binary(L) ->
    list_to_atom(binary_to_list(L));
atomize(L) ->
    L.

map_to_event(#{type := Type,
               caller_id := CallerId,
               local_id := LocalId,
               name := Name,
               reads := Reads,
               writes := Writes,
               time := Time}) ->
    #event{type=atomize(Type),
           caller_id=atomize(CallerId),
           local_id=atomize(LocalId),
           name=atomize(Name),
           reads=lists:map(fun atomize/1, Reads),
           writes=lists:map(fun atomize/1, Writes),
           time=Time};
map_to_event(#{type := Type,
               caller_id := CallerId,
               local_id := LocalId,
               amount := Amount,
               time := Time}) ->
    #dc_event{type=atomize(Type),
              caller_id=atomize(CallerId),
              local_id=atomize(LocalId),
              amount=Amount,
              time=Time}.

event_to_map(#event{type=Type,
                    caller_id=CallerId,
                    local_id=LocalId,
                    name=Name,
                    reads=Reads,
                    writes=Writes,
                    time=Time}) ->
    #{type => Type,
      caller_id => CallerId,
      local_id => LocalId,
      name => Name,
      reads => Reads,
      writes => Writes,
      time => Time};
event_to_map(#dc_event{type=Type,
                       caller_id=CallerId,
                       local_id=LocalId,
                       amount=Amount,
                       time=Time}) ->
    #{type => Type,
      caller_id => CallerId,
      local_id => LocalId,
      amount => Amount,
      time => Time}.

construct_local_trace(LocalTrace) ->
    lists:map(fun map_to_event/1, LocalTrace).

json_to_trace(JSON) ->
    RawTrace = jsx:decode(JSON, [{labels, atom}, return_maps]),
    lists:foldl(fun (#{cog_id := Id, cog_schedule := LocalTrace}, Acc) ->
                        maps:put(Id, construct_local_trace(LocalTrace), Acc)
                end, #{}, RawTrace).

json_to_scheduling_trace(JSON) ->
    Trace = json_to_trace(JSON),
    maps:map(fun(Cog, LocalTrace) ->
                     lists:filter(fun (E=#event{type=schedule}) -> true;
                                      (E=#dc_event{}) -> true;
                                      (E) -> false
                                  end, LocalTrace)
             end, Trace).

local_trace_to_json_friendly(LocalTrace) ->
    lists:map(fun event_to_map/1, LocalTrace).

trace_to_json_friendly(Trace) ->
    T = maps:fold(fun (CogId, LocalTrace, Acc) ->
                          [#{cog_id => CogId,
                             cog_schedule => local_trace_to_json_friendly(LocalTrace)}
                           | Acc]
                  end, [], Trace),
    lists:reverse(T).

get_trace_json() ->
    Trace = cog_monitor:get_trace(),
    jsx:encode(trace_to_json_friendly(Trace)).

get_db_traces_json() ->
    Traces = dpor:get_traces_from_db(),
    JSONTraces = lists:map(fun trace_to_json_friendly/1, Traces),
    jsx:encode(JSONTraces).


handle_static_dcs([]) ->
    {200, <<"text/plain">> , get_statistics() }.


get_statistics() ->
    DCs = cog_monitor:get_dcs(),
    DC_infos=lists:flatten(lists:map(fun dc:get_description/1, DCs)),
    io_lib:format("Deployment components:~n~s~n", [DC_infos]).

terminate(_Reason, _Req, _State) ->
    ok.

print_statistics() ->
    io:format("~s", [get_statistics()]),
    ok.

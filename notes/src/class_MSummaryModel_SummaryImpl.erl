-module(class_MSummaryModel_SummaryImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Summary">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MSummaryModel_SummaryImpl,'id'=null,'datestamp'=null,'description'=null,'income'=null,'expense'=null,'programName'=null}).
'init_internal'()->
    #state{}.

 %% model/Summary.abs:20
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/Summary.abs:21
'get_val_internal'(#state{'datestamp'=G},'datestamp')->
    G;
 %% model/Summary.abs:22
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% model/Summary.abs:23
'get_val_internal'(#state{'income'=G},'income')->
    G;
 %% model/Summary.abs:24
'get_val_internal'(#state{'expense'=G},'expense')->
    G;
 %% model/Summary.abs:25
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/Summary.abs:20
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/Summary.abs:21
'set_val_internal'(S,'datestamp',V)->
    S#state{'datestamp'=V};
 %% model/Summary.abs:22
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% model/Summary.abs:23
'set_val_internal'(S,'income',V)->
    S#state{'income'=V};
 %% model/Summary.abs:24
'set_val_internal'(S,'expense',V)->
    S#state{'expense'=V};
 %% model/Summary.abs:25
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'datestamp', S#state.'datestamp' }
        , { 'description', S#state.'description' }
        , { 'income', S#state.'income' }
        , { 'expense', S#state.'expense' }
        , { 'programName', S#state.'programName' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/Summary.abs:20--20
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/Summary.abs:21--21
    put(this, C:set_val_internal(get(this),'datestamp',<<""/utf8>>)),
     %% model/Summary.abs:22--22
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% model/Summary.abs:23--23
    put(this, C:set_val_internal(get(this),'income',0)),
     %% model/Summary.abs:24--24
    put(this, C:set_val_internal(get(this),'expense',0)),
     %% model/Summary.abs:25--25
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/Summary.abs:27
 %% model/Summary.abs:27
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:27--27
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:28
 %% model/Summary.abs:28
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/Summary.abs:28--28
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:29
 %% model/Summary.abs:29
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:29--29
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:30
 %% model/Summary.abs:30
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% model/Summary.abs:30--30
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:31
 %% model/Summary.abs:31
'm_getDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:31--31
        C:get_val_internal(get(this), 'datestamp')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:32
 %% model/Summary.abs:32
'm_setDatestamp'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_datestamp_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'datestamp' => V_datestamp_0 }),
    try
         %% model/Summary.abs:32--32
        put(this, C:set_val_internal(get(this), 'datestamp',maps:get('datestamp', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDatestamp and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:33
 %% model/Summary.abs:33
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:33--33
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:34
 %% model/Summary.abs:34
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% model/Summary.abs:34--34
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:35
 %% model/Summary.abs:35
'm_getIncome'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:35--35
        C:get_val_internal(get(this), 'income')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIncome and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:36
 %% model/Summary.abs:36
'm_setIncome'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_income_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'income' => V_income_0 }),
    try
         %% model/Summary.abs:36--36
        put(this, C:set_val_internal(get(this), 'income',maps:get('income', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setIncome and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:37
 %% model/Summary.abs:37
'm_getExpense'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Summary.abs:37--37
        C:get_val_internal(get(this), 'expense')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getExpense and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Summary.abs:38
 %% model/Summary.abs:38
'm_setExpense'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_expense_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'expense' => V_expense_0 }),
    try
         %% model/Summary.abs:38--38
        put(this, C:set_val_internal(get(this), 'expense',maps:get('expense', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setExpense and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

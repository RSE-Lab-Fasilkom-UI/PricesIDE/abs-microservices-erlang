-module(class_MChartOfAccountModel_ChartOfAccountImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ChartOfAccount">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MChartOfAccountModel_ChartOfAccountImpl,'id'=null,'name'=null,'description'=null,'reference'=null}).
'init_internal'()->
    #state{}.

 %% model/ChartOfAccount.abs:16
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/ChartOfAccount.abs:17
'get_val_internal'(#state{'name'=G},'name')->
    G;
 %% model/ChartOfAccount.abs:18
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% model/ChartOfAccount.abs:19
'get_val_internal'(#state{'reference'=G},'reference')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/ChartOfAccount.abs:16
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/ChartOfAccount.abs:17
'set_val_internal'(S,'name',V)->
    S#state{'name'=V};
 %% model/ChartOfAccount.abs:18
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% model/ChartOfAccount.abs:19
'set_val_internal'(S,'reference',V)->
    S#state{'reference'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'name', S#state.'name' }
        , { 'description', S#state.'description' }
        , { 'reference', S#state.'reference' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/ChartOfAccount.abs:16--16
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/ChartOfAccount.abs:17--17
    put(this, C:set_val_internal(get(this),'name',<<""/utf8>>)),
     %% model/ChartOfAccount.abs:18--18
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% model/ChartOfAccount.abs:19--19
    put(this, C:set_val_internal(get(this),'reference',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/ChartOfAccount.abs:21
 %% model/ChartOfAccount.abs:21
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccount.abs:21--21
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:22
 %% model/ChartOfAccount.abs:22
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/ChartOfAccount.abs:22--22
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:23
 %% model/ChartOfAccount.abs:23
'm_getName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccount.abs:23--23
        C:get_val_internal(get(this), 'name')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:24
 %% model/ChartOfAccount.abs:24
'm_setName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0 }),
    try
         %% model/ChartOfAccount.abs:24--24
        put(this, C:set_val_internal(get(this), 'name',maps:get('name', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:25
 %% model/ChartOfAccount.abs:25
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccount.abs:25--25
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:26
 %% model/ChartOfAccount.abs:26
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% model/ChartOfAccount.abs:26--26
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:27
 %% model/ChartOfAccount.abs:27
'm_getReference'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccount.abs:27--27
        C:get_val_internal(get(this), 'reference')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getReference and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccount.abs:28
 %% model/ChartOfAccount.abs:28
'm_setReference'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_reference_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'reference' => V_reference_0 }),
    try
         %% model/ChartOfAccount.abs:28--28
        put(this, C:set_val_internal(get(this), 'reference',maps:get('reference', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setReference and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

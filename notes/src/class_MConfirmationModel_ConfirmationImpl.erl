-module(class_MConfirmationModel_ConfirmationImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Confirmation">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MConfirmationModel_ConfirmationImpl,'id'=null,'programName'=null,'accountNumber'=null,'amount'=null}).
'init_internal'()->
    #state{}.

 %% model/Confirmation.abs:16
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/Confirmation.abs:17
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
 %% model/Confirmation.abs:18
'get_val_internal'(#state{'accountNumber'=G},'accountNumber')->
    G;
 %% model/Confirmation.abs:19
'get_val_internal'(#state{'amount'=G},'amount')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/Confirmation.abs:16
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/Confirmation.abs:17
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V};
 %% model/Confirmation.abs:18
'set_val_internal'(S,'accountNumber',V)->
    S#state{'accountNumber'=V};
 %% model/Confirmation.abs:19
'set_val_internal'(S,'amount',V)->
    S#state{'amount'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'programName', S#state.'programName' }
        , { 'accountNumber', S#state.'accountNumber' }
        , { 'amount', S#state.'amount' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/Confirmation.abs:16--16
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/Confirmation.abs:17--17
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
     %% model/Confirmation.abs:18--18
    put(this, C:set_val_internal(get(this),'accountNumber',<<""/utf8>>)),
     %% model/Confirmation.abs:19--19
    put(this, C:set_val_internal(get(this),'amount',0)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/Confirmation.abs:21
 %% model/Confirmation.abs:21
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Confirmation.abs:21--21
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:22
 %% model/Confirmation.abs:22
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/Confirmation.abs:22--22
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:23
 %% model/Confirmation.abs:23
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Confirmation.abs:23--23
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:24
 %% model/Confirmation.abs:24
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% model/Confirmation.abs:24--24
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:25
 %% model/Confirmation.abs:25
'm_getAccountNumber'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Confirmation.abs:25--25
        C:get_val_internal(get(this), 'accountNumber')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAccountNumber and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:26
 %% model/Confirmation.abs:26
'm_setAccountNumber'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_accountNumber_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'accountNumber' => V_accountNumber_0 }),
    try
         %% model/Confirmation.abs:26--26
        put(this, C:set_val_internal(get(this), 'accountNumber',maps:get('accountNumber', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAccountNumber and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:27
 %% model/Confirmation.abs:27
'm_getAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Confirmation.abs:27--27
        C:get_val_internal(get(this), 'amount')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Confirmation.abs:28
 %% model/Confirmation.abs:28
'm_setAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_amount_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'amount' => V_amount_0 }),
    try
         %% model/Confirmation.abs:28--28
        put(this, C:set_val_internal(get(this), 'amount',maps:get('amount', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

-module(class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ChartOfAccountEntry">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,'idChartOfAccount'=null,'name'=null,'amount'=null,'description'=null,'level'=null}).
'init_internal'()->
    #state{}.

 %% model/ChartOfAccountEntry.abs:19
'get_val_internal'(#state{'idChartOfAccount'=G},'idChartOfAccount')->
    G;
 %% model/ChartOfAccountEntry.abs:20
'get_val_internal'(#state{'name'=G},'name')->
    G;
 %% model/ChartOfAccountEntry.abs:21
'get_val_internal'(#state{'amount'=G},'amount')->
    G;
 %% model/ChartOfAccountEntry.abs:22
'get_val_internal'(#state{'description'=G},'description')->
    G;
 %% model/ChartOfAccountEntry.abs:23
'get_val_internal'(#state{'level'=G},'level')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/ChartOfAccountEntry.abs:19
'set_val_internal'(S,'idChartOfAccount',V)->
    S#state{'idChartOfAccount'=V};
 %% model/ChartOfAccountEntry.abs:20
'set_val_internal'(S,'name',V)->
    S#state{'name'=V};
 %% model/ChartOfAccountEntry.abs:21
'set_val_internal'(S,'amount',V)->
    S#state{'amount'=V};
 %% model/ChartOfAccountEntry.abs:22
'set_val_internal'(S,'description',V)->
    S#state{'description'=V};
 %% model/ChartOfAccountEntry.abs:23
'set_val_internal'(S,'level',V)->
    S#state{'level'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'idChartOfAccount', S#state.'idChartOfAccount' }
        , { 'name', S#state.'name' }
        , { 'amount', S#state.'amount' }
        , { 'description', S#state.'description' }
        , { 'level', S#state.'level' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/ChartOfAccountEntry.abs:19--19
    put(this, C:set_val_internal(get(this),'idChartOfAccount',0)),
     %% model/ChartOfAccountEntry.abs:20--20
    put(this, C:set_val_internal(get(this),'name',<<""/utf8>>)),
     %% model/ChartOfAccountEntry.abs:21--21
    put(this, C:set_val_internal(get(this),'amount',0)),
     %% model/ChartOfAccountEntry.abs:22--22
    put(this, C:set_val_internal(get(this),'description',<<""/utf8>>)),
     %% model/ChartOfAccountEntry.abs:23--23
    put(this, C:set_val_internal(get(this),'level',0)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/ChartOfAccountEntry.abs:25
 %% model/ChartOfAccountEntry.abs:25
'm_getIdChartOfAccount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccountEntry.abs:25--25
        C:get_val_internal(get(this), 'idChartOfAccount')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getIdChartOfAccount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:26
 %% model/ChartOfAccountEntry.abs:26
'm_setIdChartOfAccount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_idChartOfAccount_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'idChartOfAccount' => V_idChartOfAccount_0 }),
    try
         %% model/ChartOfAccountEntry.abs:26--26
        put(this, C:set_val_internal(get(this), 'idChartOfAccount',maps:get('idChartOfAccount', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setIdChartOfAccount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:27
 %% model/ChartOfAccountEntry.abs:27
'm_getName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccountEntry.abs:27--27
        C:get_val_internal(get(this), 'name')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:28
 %% model/ChartOfAccountEntry.abs:28
'm_setName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0 }),
    try
         %% model/ChartOfAccountEntry.abs:28--28
        put(this, C:set_val_internal(get(this), 'name',maps:get('name', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:29
 %% model/ChartOfAccountEntry.abs:29
'm_getAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccountEntry.abs:29--29
        C:get_val_internal(get(this), 'amount')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:30
 %% model/ChartOfAccountEntry.abs:30
'm_setAmount'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_amount_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'amount' => V_amount_0 }),
    try
         %% model/ChartOfAccountEntry.abs:30--30
        put(this, C:set_val_internal(get(this), 'amount',maps:get('amount', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAmount and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:31
 %% model/ChartOfAccountEntry.abs:31
'm_getDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccountEntry.abs:31--31
        C:get_val_internal(get(this), 'description')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:32
 %% model/ChartOfAccountEntry.abs:32
'm_setDescription'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_description_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'description' => V_description_0 }),
    try
         %% model/ChartOfAccountEntry.abs:32--32
        put(this, C:set_val_internal(get(this), 'description',maps:get('description', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setDescription and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:33
 %% model/ChartOfAccountEntry.abs:33
'm_getLevel'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/ChartOfAccountEntry.abs:33--33
        C:get_val_internal(get(this), 'level')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getLevel and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/ChartOfAccountEntry.abs:34
 %% model/ChartOfAccountEntry.abs:34
'm_setLevel'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_level_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'level' => V_level_0 }),
    try
         %% model/ChartOfAccountEntry.abs:34--34
        put(this, C:set_val_internal(get(this), 'level',maps:get('level', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setLevel and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

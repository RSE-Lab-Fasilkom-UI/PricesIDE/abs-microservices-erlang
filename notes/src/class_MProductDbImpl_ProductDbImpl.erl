-module(class_MProductDbImpl_ProductDbImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ProductDb">>, <<"Object">> ].

exported() -> #{  }.
table()->"productimpl".
%% --- Internal state and low-level accessors

-record(state,{'class'=class_MProductDbImpl_ProductDbImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% repository/ProductDb.abs:22
 %% repository/ProductDb.abs:22
'm_findAllByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/ProductDb.abs:23--23
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAllByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:25
 %% repository/ProductDb.abs:25
'm_findAll'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/ProductDb.abs:26--26
        % []
        orm:findAll(table())
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAll and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:28
 %% repository/ProductDb.abs:28
'm_findByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/ProductDb.abs:29--29
        put(vars, (get(vars))#{'newClassName' => maps:get('className', get(vars))}),
         %% repository/ProductDb.abs:30--30
        put(vars, (get(vars))#{'newQuery' => maps:get('query', get(vars))}),
         %% repository/ProductDb.abs:31--31
        object:new_local(O, Cog,class_MProductModel_ProductImpl,[[O,DC| Stack]]),
        orm:findByAttributes(table(),binary_to_list(maps:get('newQuery', get(vars))))
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:33
 %% repository/ProductDb.abs:33
'm_find'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/ProductDb.abs:34--34
        object:new_local(O, Cog,class_MProductModel_ProductImpl,[[O,DC| Stack]])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method find and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:36
 %% repository/ProductDb.abs:36
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_object_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'object' => V_object_0 }),
    try
         %% repository/ProductDb.abs:37--37
        put(vars, (get(vars))#{'name' => (fun() -> case maps:get('object', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getName'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getName'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getName',[[]],#task_info{method= <<"getName"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% repository/ProductDb.abs:38--38
        put(vars, (get(vars))#{'description' => (fun() -> case maps:get('object', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getDescription'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getDescription'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getDescription',[[]],#task_info{method= <<"getDescription"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
        Name = binary_to_list(maps:get('name', get(vars))),
        Description = binary_to_list(maps:get('description', get(vars))),
        orm:saveProduct(table(), Name, Description)
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:40
 %% repository/ProductDb.abs:40
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_condition_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'condition' => V_condition_0 }),
    try
        Condition = binary_to_list(maps:get('condition', get(vars))),
        orm:delete(table(), Condition),
        ok,
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ProductDb.abs:44
 %% repository/ProductDb.abs:44
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,V_description_0,V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0,
 'description' => V_description_0,
 'id' => V_id_0 }),
    try
         %% repository/ProductDb.abs:43--43
        object:new_local(O, Cog,class_MProductModel_ProductImpl,[[O,DC| Stack]]),
        Name = binary_to_list(maps:get('name', get(vars))),
        Description = binary_to_list(maps:get('description', get(vars))),
        Id = binary_to_list(maps:get('id', get(vars))),
        orm:updateProduct(table(), "name", Name, "description", Description, "id="++Id)
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.

 %% repository/ProductDb.abs:51
 %% repository/ProductDb.abs:51
'm_log'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_log_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'log' => V_log_0 }),
    try
         %% repository/ProductDb.abs:52--52
        <<"return log"/utf8>>
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method log and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.